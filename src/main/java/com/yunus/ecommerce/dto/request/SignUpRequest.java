package com.yunus.ecommerce.dto.request;

import com.yunus.ecommerce.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {
    private String userEmail;
    private String userPassword;
    private String userAddress;
    private Role role;
}
