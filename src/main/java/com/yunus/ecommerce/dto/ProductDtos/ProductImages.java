package com.yunus.ecommerce.dto.ProductDtos;

import com.yunus.ecommerce.entity.Product;
import lombok.Data;

@Data
public class ProductImages {
    private Long id;
    private  String name;
    private  String type;
    private  byte[] picByte;
}
