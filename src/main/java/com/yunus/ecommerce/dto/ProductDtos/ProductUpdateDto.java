package com.yunus.ecommerce.dto.ProductDtos;

import com.yunus.ecommerce.entity.Image;
import lombok.Data;

import java.util.Set;

@Data
public class ProductUpdateDto {
    private Long productId;
    private String productName;
    private String productDesc;
    private Float productPrice;
    private Long productCategoryId;
    private Set<Image> productImages;
}
