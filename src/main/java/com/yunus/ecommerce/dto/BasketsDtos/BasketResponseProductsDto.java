package com.yunus.ecommerce.dto.BasketsDtos;

import com.yunus.ecommerce.dto.ProductDtos.ProductDto;
import lombok.Data;


@Data
public class BasketResponseProductsDto {
    private Long basketId;
    private Integer numberOfProducts;
    private Float totalProductPrice;
    private BasketResponseProductsInfoDto product;
}
