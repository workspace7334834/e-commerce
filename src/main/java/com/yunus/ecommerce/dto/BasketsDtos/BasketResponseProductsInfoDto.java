package com.yunus.ecommerce.dto.BasketsDtos;

import com.yunus.ecommerce.dto.ProductDtos.ProductDto;
import com.yunus.ecommerce.dto.ProductDtos.ProductImages;
import lombok.Data;

import java.util.Set;


@Data
public class BasketResponseProductsInfoDto {
        private Long productId;
        private String productName;
        private String productDesc;
        private Float productPrice;
        private Long productCategoryId;
}