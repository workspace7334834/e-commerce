package com.yunus.ecommerce.controller;

import com.nimbusds.jose.util.Resource;
import com.yunus.ecommerce.dto.ProductDtos.ProductCreateDto;
import com.yunus.ecommerce.dto.ProductDtos.ProductDto;
import com.yunus.ecommerce.dto.ProductDtos.ProductUpdateDto;
import com.yunus.ecommerce.entity.Image;
import com.yunus.ecommerce.entity.Product;
import com.yunus.ecommerce.repository.ImageRepository;
import com.yunus.ecommerce.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping(value = "/product")
public class ProductController {
    private final ProductService productService;



    private final ImageRepository imageRepository;

    public ProductController(ProductService productService,  ImageRepository imageRepository) {
        this.productService = productService;
        this.imageRepository = imageRepository;
    }

    @GetMapping("/{productId}")
    public ProductDto getProductById(@PathVariable long productId) {
        return productService.getProductById(productId);
    }

    @GetMapping("/products")
    public List<ProductDto> getProducts(){
        return productService.getProducts();
    }

    @GetMapping("/byCategory/{categoryId}")
    public List<ProductDto> getProductsByCategory(@PathVariable long categoryId){
        return productService.getProductsByCategory(categoryId);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(value = "/create", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ProductDto createProduct(@RequestPart("product") ProductCreateDto productCreateDto,
                                    @RequestPart("imageFile")MultipartFile[] file) throws IOException {
        return productService.createProduct(productCreateDto,file);
    }
    @GetMapping("/take/{id}")
    public HttpEntity<byte[]> getOneImage(@PathVariable Long id){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(productService.getOneImage(id), httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/takeallphoto/{id}")
    public ResponseEntity<List<String>> getAllImages(@PathVariable Long id) {
        List<byte[]> images = productService.getAllImages(id);

        List<String> base64Images = new ArrayList<>();
        for (byte[] image : images) {
            base64Images.add(Base64.getEncoder().encodeToString(image));
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(base64Images, httpHeaders, HttpStatus.OK);
    }


    @GetMapping("/takeall")
    public List<Image> getImages(){
        return productService.getImages();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping(value = "/update", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ProductDto updateProduct(@RequestPart("product") ProductUpdateDto productUpdateDto,
                                    @RequestPart("imageFile")MultipartFile[] file) throws IOException  {
        return productService.updateProduct(productUpdateDto, file);
    }

    @DeleteMapping("/delete")
    public String  deleteProduct(@RequestParam long productId) {
        return productService.deleteProduct(productId);
    }








}

