package com.yunus.ecommerce.service;

import com.yunus.ecommerce.dto.ProductDtos.ProductCreateDto;
import com.yunus.ecommerce.dto.ProductDtos.ProductDto;
import com.yunus.ecommerce.dto.ProductDtos.ProductUpdateDto;
import com.yunus.ecommerce.entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ProductService {
    ProductDto getProductById(long productId);
    List<ProductDto> getProducts();
    List<ProductDto> getProductsByCategory(long categoryId);
    ProductDto createProduct(ProductCreateDto productCreateDto, MultipartFile[] file) throws IOException;

    byte[] getOneImage(Long id);

    List<byte[]> getAllImages(Long id);

    List<Image> getImages();

    ProductDto updateProduct(ProductUpdateDto productUpdateDto, MultipartFile[] file) throws IOException;
    String  deleteProduct(long productId);

}
