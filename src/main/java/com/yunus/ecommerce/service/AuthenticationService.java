package com.yunus.ecommerce.service;

import com.yunus.ecommerce.dto.request.SignUpRequest;
import com.yunus.ecommerce.dto.request.SigninRequest;
import com.yunus.ecommerce.dto.response.JwtAuthenticationResponse;

public interface AuthenticationService {
    JwtAuthenticationResponse signup(SignUpRequest request);

    JwtAuthenticationResponse signin(SigninRequest request);
}
