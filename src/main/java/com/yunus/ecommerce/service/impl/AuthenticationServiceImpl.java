package com.yunus.ecommerce.service.impl;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.yunus.ecommerce.dto.request.SignUpRequest;
import com.yunus.ecommerce.dto.request.SigninRequest;
import com.yunus.ecommerce.dto.response.JwtAuthenticationResponse;
import com.yunus.ecommerce.entity.Role;
import com.yunus.ecommerce.entity.User;
import com.yunus.ecommerce.repository.UserRepository;
import com.yunus.ecommerce.service.AuthenticationService;
import com.yunus.ecommerce.service.JwtService;
import lombok.RequiredArgsConstructor;

import java.io.Console;


@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    @Override
    public JwtAuthenticationResponse signup(SignUpRequest request) {
        var user = User.builder()
                .userEmail(request.getUserEmail())
                .userAddress(request.getUserAddress())
                .userPassword(passwordEncoder.encode(request.getUserPassword()))
                .role(request.getRole()).build();
        userRepository.save(user);
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    @Override
    public JwtAuthenticationResponse signin(SigninRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUserEmail(),
                        request.getUserPassword()));
        var user = userRepository.findByUserEmail(request.getUserEmail())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }
}
