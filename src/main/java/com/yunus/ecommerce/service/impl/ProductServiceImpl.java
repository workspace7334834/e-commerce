package com.yunus.ecommerce.service.impl;


import com.yunus.ecommerce.dto.ProductDtos.ProductCreateDto;
import com.yunus.ecommerce.dto.ProductDtos.ProductDto;
import com.yunus.ecommerce.dto.ProductDtos.ProductUpdateDto;
import com.yunus.ecommerce.entity.Category;
import com.yunus.ecommerce.entity.Image;
import com.yunus.ecommerce.entity.Product;
import com.yunus.ecommerce.repository.CategoryRepository;
import com.yunus.ecommerce.repository.ImageRepository;
import com.yunus.ecommerce.repository.ProductRepository;
import com.yunus.ecommerce.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;

    private final ModelMapper modelMapper;

    public ProductServiceImpl(ProductRepository productRepository, CategoryRepository categoryRepository, ImageRepository imageRepository, ModelMapper modelMapper) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.modelMapper = modelMapper;

    }

    public ProductDto getProductById(long productId){
        String errorMessage = "Product does not exist";
        Product product = this.productRepository.findById(productId)
                .orElseThrow(() -> new DataIntegrityViolationException(errorMessage));

        return this.modelMapper.map(product,ProductDto.class) ;
    }

    public List<ProductDto> getProducts(){
        List<Product> products = productRepository.findAll();
        return products.stream()
                .map(product -> modelMapper.map(product,ProductDto.class))
                .collect(Collectors.toList());
    }

    public List<ProductDto> getProductsByCategory(long categoryId){
        List<Product> products = productRepository.findByCategoryId(categoryId);
        return products.stream()
                .map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toList());
    }




    public ProductDto createProduct(ProductCreateDto productCreateDto, MultipartFile[] file) throws IOException {


        Product product = new Product();
        product.setProductName(productCreateDto.getProductName());
        product.setProductDesc(productCreateDto.getProductDesc());
        Category category = categoryRepository.findById(productCreateDto.getProductCategoryId()).orElse(null);
        product.setCategory_id(category);
        product.setProductPrice(productCreateDto.getProductPrice());

        try{
            productRepository.save(product);
        } catch (DataIntegrityViolationException ex){
            String errorMessage = "Failed to create product.";
            throw new DataIntegrityViolationException(errorMessage);
        }

        System.out.println(product.getProductId());

        List<Image> images = uploadImage(file, product.getProductId() );
        product.setImages(images);



        try{
            productRepository.save(product);
            return this.modelMapper.map(product,ProductDto.class);
        } catch (DataIntegrityViolationException ex){
            String errorMessage = "Failed to create product.";
            throw new DataIntegrityViolationException(errorMessage);
        }
    }

    public List<Image> uploadImage(MultipartFile[] multipartfiles,Long id) throws IOException {

        List<Image> imageModels = new ArrayList<>();

        for (MultipartFile file: multipartfiles) {
            Image imageModel = new Image(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getBytes()
            );
            ProductDto productDto = getProductById(id);
            Product product = this.modelMapper.map(productDto, Product.class);

            imageModel.setProduct(product);
            imageModels.add(imageModel);
            imageRepository.save(imageModel);
        }

        return imageModels;
    }

    @Override
    public byte[] getOneImage(Long id) {
        //imageRepository.findByProductProductId(id);
        
        List<byte[]> images = imageRepository.findImagesByProductId(id);
        byte[] firstImage = new byte[0];
                
        if (!images.isEmpty()) {
            firstImage = images.get(0);
            // Do something with the firstImage
        } else {
            // Handle the case where no images are found
        }

        return firstImage;
    }
    @Override
    public List<byte[]> getAllImages(Long id) {
        List<byte[]> images = imageRepository.findImagesByProductId(id);

        return images;
    }
    public List<Image> getImages(){
        List<Image> images = imageRepository.findAll();
        return images.stream().collect(Collectors.toList());
    }

    public ProductDto updateProduct(ProductUpdateDto productUpdateDto, MultipartFile[] file) throws IOException {
        ProductDto productDto = getProductById(productUpdateDto.getProductId());
        Product product = this.modelMapper.map(productDto, Product.class);

        try {
            product.setProductName(productUpdateDto.getProductName());
            product.setProductPrice(productUpdateDto.getProductPrice());
            product.setProductDesc(productUpdateDto.getProductDesc());
            Category category = categoryRepository.findById(productUpdateDto.getProductCategoryId()).orElse(null);
            product.setCategory_id(category);

            List<Image> images = uploadImage(file, product.getProductId() );
            product.setImages(images);


            productRepository.save(product);

            return this.modelMapper.map(product,ProductDto.class);
        } catch (DataIntegrityViolationException ex){
            String errorMessage = "Product failed to update";
            throw new DataIntegrityViolationException(errorMessage);
        }
    }

    public String  deleteProduct(long productId) {
        Optional<Product> product = productRepository.findById(productId);

        if(product.isPresent() ){
            productRepository.deleteById(productId);
            return "The product deleted.";
        }else {
            return "Product not found.";
        }
    }

    


}
