package com.yunus.ecommerce.repository;

import com.yunus.ecommerce.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,Long> {

    @Query("SELECT p FROM Order p WHERE p.user.userId = :userId")
    List<Order> findByUserId(long userId);
}
