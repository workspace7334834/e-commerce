package com.yunus.ecommerce.repository;


import com.yunus.ecommerce.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    //User findByUserEmail(String userEmail);

    Optional<User> findByUserEmail(String email);


}
