package com.yunus.ecommerce.repository;

import com.yunus.ecommerce.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {



    @Query("SELECT i.picByte FROM Image i WHERE i.product.productId = :productId")
    List<byte[]> findImagesByProductId(@Param("productId") Long productId);






}

