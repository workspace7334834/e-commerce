package com.yunus.ecommerce.entity;

public enum Role {
    USER,
    ADMIN
}
