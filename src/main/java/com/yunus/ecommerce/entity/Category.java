package com.yunus.ecommerce.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "category" , schema = "e_commerce")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "category_name")
    private String categoryName;

    @OneToMany(mappedBy = "category_id", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Product> products;

}
