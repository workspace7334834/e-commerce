package com.yunus.ecommerce.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import java.util.List;

@Entity
@Table(name = "product", schema = "e_commerce")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long productId;

    @Column(name = "product_name", nullable = false)
    private  String productName;

    @Column(name = "product_desc")
    private  String productDesc;

    @Column(name = "product_price", nullable = false)
    private  Float productPrice;



    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Image> images;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id",nullable = false)
    private  Category category_id;

    @OneToMany(mappedBy = "basketProduct", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Basket> basketProducts;

    @OneToMany(mappedBy = "orderProduct", fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE) // Kaskad ayarını değiştirin
    private List<OrderDetail> order_details;

}
